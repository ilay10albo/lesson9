#include "BSNode.h"
int BSNode::_vertical_index;
BSNode::BSNode(string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	_count = 1;
	helpVerical();

}

BSNode::BSNode(const BSNode & other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	_count = 1;
	helpVerical();

}

BSNode::~BSNode()
{
	if (this->_left != nullptr)
	{
		delete this->_left;
	}
	if (this->_right != nullptr)
	{
		delete this->_right;
	}
}

void BSNode::insert(string value)
{
		if (strcmp(value.c_str(), this->_data.c_str()) <= 0)
		{
			if (this->_left != nullptr)
			{
				if (search(value))
				{
					_left->_count++;
					return;
				}
				_left->insert(value);
			}
			else
			{
				this->_left = new BSNode(value);
				this->_count++;
			}
		}
		else
		{
			if (this->_right != nullptr)
			{
				if (search(value))
				{
					_right->_count++;
					return;
				}
				_right->insert(value);
				
			}
			else
			{
				this->_right = new BSNode(value);
				this->_count++; 
			}
		}
	
}

BSNode & BSNode::operator=(const BSNode & other)
{
	this->_data = other._data;
	this->_left = other._left;
	this->_right = other._right;
	return *this;
}

bool BSNode::isLeaf() const
{
	return this->_left == nullptr && this->_right == nullptr;
}

string BSNode::getData() const
{
	return this->_data;
}

BSNode * BSNode::getLeft() const
{
	return this->_left;
}

BSNode * BSNode::getRight() const
{
	return this->_right;
}

bool BSNode::search(string val) const
{
	string right;
	string left;
	if (strcmp(_data.c_str(),val.c_str()) == 0) 
	{
		return true;
	}
	if (strcmp(_data.c_str(), val.c_str()) > 0)
	{
		if ((getLeft())!=nullptr)
		{
			return (getLeft())->search(val);
		}
		else
		{
			return false;
		}

	}
	else
	{
		if ((getRight()) != nullptr)
		{
			return (getRight())->search(val);
		}
		else
		{
			return false;
		}
	}
	


}

int BSNode::getHeight() const
{
	int right = 0;
	int left = 0;

	if (getRight() != nullptr)
	{
		right = 1 + (getRight())->getHeight();
	}
	if (getLeft() != nullptr)
	{
		left = 1 + (getLeft())->getHeight();
	}
	if (left < right)
	{
		return right;
	}return left;
	
}

int BSNode::getDepth(const BSNode & root) const
{
	if (!root.search(this->_data))
	{
		return -1; // Check if value exist
	}

	return root.getHeight() - getHeight();
	
}

void BSNode::printNodes() const
{
	if (this == nullptr)
	{
		return;
	}
	
	_left->printNodes();
	cout << BSNode::_vertical_index << ". " << this->_data << endl;
	BSNode::_vertical_index++;

	_right->printNodes();

	


}

void BSNode::helpVerical()
{
	if (_vertical_index != 0)
	{
		BSNode::_vertical_index = 0;
	}
}
