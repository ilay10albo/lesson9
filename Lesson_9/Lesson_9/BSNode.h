#ifndef BSNode_H
#define BSNode_H

#include <string>
#include <iostream>
using namespace std;

class BSNode
{
private:
	string _data;
	BSNode* _left;
	BSNode* _right;
public:
	static int  _vertical_index;

	BSNode(string data); // Build node
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(string value); // Insert node
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	string getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(string val) const; // Seach if a values in current subnode

	int getHeight() const; // Get hieght of a node 
	int getDepth(const BSNode& root) const; // Get depth

	void printNodes() const; // Print node in sequnce
	void helpVerical(); // Side function
	int _count; 
};

#endif