#pragma once
template <class TYPE>

int compare(TYPE a, TYPE b) // Compare values
{
	if (a==b)
	{
		return 0;
	}
	if (a>b)
	{
		return 1;
	}
	return-1;
}
template <class TYPE>

void bubbleSort(TYPE arr[], int n) // Bubble sort
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}
		if (swapped == false)
			break;
	}
}

template <class TYPE>

void swap(TYPE *xp, TYPE *yp) // Swap values
{
	TYPE temp = *xp;
	*xp = *yp;
	*yp = temp;
}
template <class TYPE>

void printArray(TYPE arr[], int size) // Print array
{
	int i;
	for (i = 0; i < size; i++)
		std::cout << arr[i] << " " ;
	printf("\n");
}