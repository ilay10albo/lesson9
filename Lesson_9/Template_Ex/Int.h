#pragma once
#include <ostream>
#include <stdio.h>
using namespace std;

class Int
{
	
public:
	int _data;
	Int(int _dat);
	~Int();
	void operator=(const Int& other);
	bool operator==(const Int& other);
	bool operator>(const Int& other);
	bool operator<(const Int& other);
	bool operator>=(const Int& other);
	bool operator<=(const Int& other);
	void operator-=(const Int& other);
	void operator+=(const Int& other);
	friend ostream& operator<<(std::ostream& os, const Int& obj);

};

