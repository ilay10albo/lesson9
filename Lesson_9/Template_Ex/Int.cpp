#include "Int.h"



Int::Int(int _dat)
{
	this->_data = _dat;
}


Int::~Int()
{
}

void Int::operator=(const Int & other)
{
	this->_data = other._data;
}

bool Int::operator==(const Int & other)
{
	return this->_data == other._data;
}

bool Int::operator>(const Int & other)
{
	return this->_data > other._data;
}

bool Int::operator<(const Int & other)
{
	return this->_data < other._data;
}

bool Int::operator>=(const Int & other)
{
	return this->_data >= other._data;
}

bool Int::operator<=(const Int & other)
{
	return this->_data <= other._data;;
}
void Int::operator-=(const Int & other)
{
	this->_data = this->_data - other._data;
}
void Int::operator+=(const Int & other)
{
	this->_data = this->_data + other._data;
}
std::ostream& operator<<(std::ostream& os, const Int& obj)
{
	os << obj._data;
	return os;
}