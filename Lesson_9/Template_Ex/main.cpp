#include "functions.h"
#include <iostream>
#include <stdio.h>
#include <string>
#include "Int.h"
using namespace std;
int main() {
	
//check compare

	std::cout << "correct print is -1 1 0" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

// check char
	cout << "Checking char:" << endl;
	std::cout << compare<char>('a', '9') << std::endl;
	std::cout << compare<char>('b', 'z') << std::endl;
	std::cout << compare<char>('a', 'a') << std::endl;
// check my member
	cout << "Checking personal Int member" << endl;
	std::cout << compare<Int>(Int(5), Int(10)) << std::endl;
	std::cout << compare<Int>(Int(6), Int(5)) << std::endl;
	std::cout << compare<Int>(Int(5), Int(5)) << std::endl;
//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;


//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;

	cout << "Array of Int members" << endl;
	Int IntArray[arr_size] = { Int(7),Int(55),Int(3),Int(4),Int(1) };
	printArray<Int>(IntArray, arr_size);
	cout << "Array of sorted Int members" << endl;
	bubbleSort<Int>(IntArray, arr_size);
	printArray<Int>(IntArray, arr_size);
	system("pause");
	return 1;
}