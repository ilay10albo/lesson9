#pragma once
template <class T>

class BSNode
{
private:
	T _data;
	BSNode* _left;
	BSNode* _right;
public:
	static int  _vertical_index;

	BSNode(T data)
	{
		this->_data = data;
		this->_left = nullptr;
		this->_right = nullptr;
	}
	BSNode(const BSNode& other)
	{
		this->_data = other._data;
		this->_left = other._left;
		this->_right = other._right;
		_count = 1;
	}

	~BSNode()
	{
		if (this->_left != nullptr)
		{
			delete this->_left;
		}
		if (this->_right != nullptr)
		{
			delete this->_right;
		}
	}

	void insert(T value)
	{
		if (value <= this->_data )
		{
			if (this->_left != nullptr)
			{
				if (search(value))
				{
					_left->_count++;
					return;
				}
				_left->insert(value);
			}
			else
			{
				this->_left = new BSNode(value);
				this->_count++;
			}
		}
		else
		{
			if (this->_right != nullptr)
			{
				if (search(value))
				{
					_right->_count++;
					return;
				}
				_right->insert(value);

			}
			else
			{
				this->_right = new BSNode(value);
				this->_count++;
			}
		}
	}
	BSNode& operator=(const BSNode& other)
	{
		this->_data = other._data;
		this->_left = other._left;
		this->_right = other._right;
		return *this;
	}

	bool isLeaf() const
	{
		return this->_left == nullptr && this->_right == nullptr;
	}
	T getData() const
	{
		return this->_data;

	}
	BSNode* getLeft() const
	{
		return this->_left;
	}
	BSNode* getRight() const
	{
		return this->_right;
	}

	bool search(T val) const
	{
		T right;
		T left;
		if (_data == val)
		{
			return true;
		}
		if (_data> val)
		{
			if ((getLeft()) != nullptr)
			{
				return (getLeft())->search(val);
			}
			else
			{
				return false;
			}

		}
		else
		{
			if ((getRight()) != nullptr)
			{
				return (getRight())->search(val);
			}
			else
			{
				return false;
			}
		}
	}

	int getHeight() const
	{
		int right = 0;
		int left = 0;

		if (getRight() != nullptr)
		{
			right = 1 + (getRight())->getHeight();
		}
		if (getLeft() != nullptr)
		{
			left = 1 + (getLeft())->getHeight();
		}
		if (left < right)
		{
			return right;
		}return left;
	}
	int getDepth(const BSNode& root) const
	{
		if (!root.search(this->_data))
		{
			return -1; // Check if value exist
		}
		return root.getHeight() - getHeight();
	}

	void printNodes() const
	{
		if (this == nullptr)
		{
			return;
		}
		_left->printNodes();
		cout << this->_data << " ";

		_right->printNodes();

	}//for question 1 part C
	void helpVerical()
	{
		if (_vertical_index != 0)
		{
			BSNode<T>::_vertical_index = 0;
		}
	}
	int _count;
};
