// class templates
#include <iostream>
#include <string>
#include "BSNode.h"
using namespace std;

int main() {
	int const sizeOfArray = 5;
	int inte[sizeOfArray] = {4,2,1,3,10};
	string stringe[sizeOfArray] = { "h","b","c","e","q" };
	

	cout << "[*] Printing int array " << endl;
	for (int index = 0; index < sizeOfArray; index++)
	{
		cout <<  inte[index] << " ";
	}cout << endl;
	cout << "[*] Printing string array " << endl;
	for (int index = 0; index < sizeOfArray; index++)
	{
		cout << stringe[index] << " ";
	}cout << endl;

	BSNode <string> stringNode(stringe[0]); // Set up string tree
	BSNode <int> intNode(inte[0]); // Set up int tree
	for (int i = 1; i < sizeOfArray; i++)
	{
		intNode.insert(inte[i]);
		stringNode.insert(stringe[i]);
	}
	cout << "[//] Sorted string array (By node):" << endl;
	stringNode.printNodes(); // Print sorted array from tree
	cout << "\n[//] Sorted int array (By node):" << endl;
	intNode.printNodes();
	getchar();
	return 0;
}